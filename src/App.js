import Clock from "./components/clock/Clock.jsx";
import Counter from "./components/counter/Counter.jsx";
import ControlledUncontrolled from "./ControlledUncontrolled.jsx"
import "./App.css"
import List from "./components/list/List.jsx";
import ThemeChanger from "./components/theme changer/ThemeChanger.jsx";
import { ThemeProvider } from "./contexts/theme.jsx";
import ColorBox from "./components/color box/ColorBox.jsx";



function App() {
  return (
    <div className="App">
     <Counter/>
     <Clock/>
     <ControlledUncontrolled />
     <List/>
     <ThemeProvider>
     <ThemeChanger/>
     </ThemeProvider>
     <ColorBox/>
    </div>
  );
}

export default App;
