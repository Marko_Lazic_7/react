import React, { useEffect, useState } from 'react'
import "./Clock.css"

function Clock() {
const [input, setInput] = useState("")
const [dateTime, setDateTime] = useState(new Date());
let hour = dateTime.getHours();
let greeting = "Good " + ( hour >= 8 && hour < 12 ? "Morning" : hour >= 12 && hour < 18 ? "Afternoon" : hour >= 18 && hour < 24 ?  "Evening" : "Night")

useEffect(() => {
    const id = setInterval(() => setDateTime(new Date()), 1000);
    return () => {
        clearInterval(id); 
    }
}, []);


function handleChange(event) {
    setInput(event.target.value)
}


    return (
        <div className="containerClock">
            <div className="clock">
            {dateTime.toLocaleTimeString()}
            </div>
        
        <input
          className="input"
          placeholder="First Name"
          value={input}
          name="firstName"
          onChange={handleChange}
        />

        <p className="text">{greeting}, <span className="name">{input} </span>  </p>  
        </div>
        
    )
}

export default Clock
