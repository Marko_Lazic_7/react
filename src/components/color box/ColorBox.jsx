import React, { useRef, useState } from 'react'

function ColorBox() {
    const boxRef = useRef(null)
    const changeColor = (event) => {
        boxRef.current.style.backgroundColor = event.target.value;
      };

    const [height, setHeight] = useState(100);
    const [width, setWidth] = useState(100) 

    const changeSize = () => {
        
        boxRef.current.style.height = height + 10 + "px";
        boxRef.current.style.width = width + 10 + "px";

        setHeight((height) => height + 10)
        setWidth((width) => width + 10)

    }


    const reset = () => {
        boxRef.current.style.backgroundColor = "transparent"
        boxRef.current.style.height = "100px"
        boxRef.current.style.width = "100px";
        setHeight(100)
        setWidth(100)
    }

    
    return (
        <div >
            <div className="box" ref={boxRef} style={{border: "1px solid black", height: "100px", width: "100px", backgroundColor: "transparent"}}>
            
            </div>
            <select  onChange={changeColor}>
              <option value="transparent">Transparent</option>
              <option value="red" onClick={changeColor}>Red</option>
              <option value="yellow">Yellow</option>
              <option value="grey">Grey</option>
           </select>
            <button onClick={changeSize}>Click Me!</button>
            <button onClick={reset}>Reset!</button>
        </div>
    )
}

export default ColorBox
