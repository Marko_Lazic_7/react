import React, { useContext } from 'react'
import "./ThemeChanger.css"
import { ThemeContext } from '../../contexts/theme'


function ThemeChanger() {
const [{theme, font, isDark, isBig}, toggleTheme, toggleFont] = useContext(ThemeContext)

    return (
        <div className="themeContainer" style={{backgroundColor:theme.backgroundColor, color:theme.color, fontSize: font.fontSize}}>
            <div>Theme Changer</div>
            <div>You can change theme between Dark & Light mode and font size between normal and big...
                <div>It is a {isDark ? "Dark" : "Light"} theme and {isBig ? "Big" : "Normal"} font!</div> 

            </div>
            <div className="buttonContainer">
            <button style={{backgroundColor:isDark ? "white" : "black", color:isDark ? "black" : "white"}}  onClick={toggleTheme}>Switch to {isDark ? "Light" : "Dark"}</button>
            <button style={{backgroundColor:isDark ? "white" : "black", color:isDark ? "black" : "white"}} onClick={toggleFont}>Switch to {isBig ? "Normal" : "Big"}</button>
            </div>
        </div>
    )
}

export default ThemeChanger
