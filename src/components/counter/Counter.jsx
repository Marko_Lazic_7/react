import React, { useEffect, useState } from 'react'
import  "./Counter.css";


function Counter() {
let [counter, setCounter] = useState(0);

useEffect(() => {
    const id = setInterval(
        () => setCounter(counter => counter + 1),
        5000
      );
      return () => {
        clearInterval(id);
    }
      
}, [])

    return (
        <div className="container">
            <div className="counter">{counter}</div>
        </div>
    )
}

export default Counter
