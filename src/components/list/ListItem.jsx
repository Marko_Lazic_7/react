import React from 'react'


function ListItem({username, age, registration }) {
    return (
    <div>
      <ul style={{margin: "10px"}}>
        <li>Username: {username}</li>
        <li>Age: {age}</li>
        <li>Registration Date: {registration.getUTCDate()}. {registration.getUTCMonth()}. {registration.getUTCFullYear()}.</li>
      </ul>
    </div>
    )
}

export default ListItem
