import React, { useState } from 'react'
import { v4 as uuidv4 } from 'uuid';
import ListItem from './ListItem';

const userData = [{
    username: "Marko",
    age: 34,
    registration: new Date (2020, 11, 17)
},
{
    username: "Mirko",
    age: 22,
    registration: new Date (2012, 4, 27)
},
{
    username: "Darko",
    age: 27,
    registration: new Date (2007, 7, 1)
},
{
    username: "Sam",
    age: 45,
    registration: new Date (2000, 9, 10)
},
{
    username: "Anna",
    age: 19,
    registration: new Date (2015, 1, 5)
}]


function List() {


    const [numberOfUser, setNumberOfUser] = useState(userData.length);
    const listUser = () => userData.map((user) => <ListItem username={user.username} age={user.age} registration={user.registration} key={uuidv4()}/>)
    
    return (
        <div>
           {listUser()}
            <p style={{margin: "10px"}}>Total number of users: {numberOfUser}</p>
        </div>
    )
}

export default List
