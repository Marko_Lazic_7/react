import { useState } from "react";

function Form() {
    // you can have multiple useState hooks, e.g. for each individual property
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("")
  
    function handleChangeLastName(event) {
       // note that we spread previous state
       
       setLastName(event.target.value)
    }
    function handleChangeFirstName(event) {
        // note that we spread previous state
        setFirstName(event.target.value)
        
     }
  

    return (
      <div>
        <div>Welcome, {firstName + " " + lastName}</div>
        <div>
          <label>First name:</label>
          <input
            value={firstName}
            name="firstName"
            onChange={handleChangeFirstName}
          />
        </div>
        <div>
          <label>Last name:</label>
          <input
            value={lastName}
            name="lastName"
            onChange={handleChangeLastName}
          />
        </div>
     </div>
    );
  }

  export default Form;