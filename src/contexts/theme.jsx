import { createContext, useState } from "react";

const themes = {
    dark: {
        backgroundColor: "black",
        color: "white"
    },
    light: {
        backgroundColor: "white",
        color: "black"
    }
}

const fonts = {
    normal: {
        fontSize: "12px"
    },
    big: {
        fontSize: "18px"
    }
}

export const ThemeContext = createContext();

export const ThemeProvider = ({children}) => {
const [isDark, setIsDark] = useState(false);
const [isBig, setIsBig] = useState(false);

const theme = isDark ? themes.dark : themes.light;
const font = isBig ? fonts.big : fonts.normal;

const toggleTheme = () => setIsDark(!isDark)
const toggleFont = () => setIsBig(!isBig)

    return (
<ThemeContext.Provider value={[{theme, font, isDark, isBig}, toggleTheme, toggleFont]}>{children}</ThemeContext.Provider>
    )
}